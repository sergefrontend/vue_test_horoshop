export default (function(){
  const items = [
    {
      id: 105,
      title: 'Ostrov',
      src: 'https://cdn.pixabay.com/photo/2019/11/02/20/18/fog-4597348__480.jpg'
    },
    {
      id: 104,
      title: 'Flora',
      src: 'https://cdn.pixabay.com/photo/2019/11/08/18/16/matera-4612016__340.jpg'
    },
    {
      id: 103,
      title: 'SunWear',
      src: 'https://cdn.pixabay.com/photo/2019/11/09/16/25/kristin-4613926__340.jpg'
    },
    {
      id: 102,
      title: 'Flora',
      src: 'https://cdn.pixabay.com/photo/2019/11/14/23/08/squirrel-4627334__340.jpg'
    },
    {
      id: 111,
      title: 'Ostrov',
      src: 'https://cdn.pixabay.com/photo/2019/11/07/17/07/universe-4609408__340.jpg'
    },
    {
      id: 10,
      title: 'Ostrov',
      src: 'https://cdn.pixabay.com/photo/2019/10/29/13/48/landscape-4587002__340.jpg'
    },
    {
      id: 16,
      title: 'Ostrov',
      src: 'https://cdn.pixabay.com/photo/2019/11/12/14/52/sunflower-4621226__340.jpg'
    },
    {
      id: 77,
      title: 'Ostrov',
      src: 'https://cdn.pixabay.com/photo/2019/10/22/02/37/nature-4567502__340.jpg'
    },
  ];
  return items;
})();
